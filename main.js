const noble = require('noble');

const ECHO_SERVICE_UUID = 'ec33';
const ECHO_CHARACTERISTIC_UUID = 'cc33';

noble.on('stateChange', state => {
	if (state === 'poweredOn') {
		console.log('Looking for service...');
		noble.startScanning([ECHO_SERVICE_UUID]);
	} else {
		noble.stopScanning();
	}
});

noble.on('discover', peripheral => {
	
	//noble.stopScanning();
	const name = peripheral.advertisement.localName;
    console.log(`Found ${name} ${peripheral.id}`);
	connectAndSetUp(peripheral);
});

function connectAndSetUp(peripheral) {
	
	peripheral.connect(error => {
		console.log('Connected to', peripheral.id);
		
		const serviceUUIDs = [ECHO_SERVICE_UUID];
		const characteristicUUIDs = [ECHO_CHARACTERISTIC_UUID];
		
		peripheral.discoverSomeServicesAndCharacteristics(
			serviceUUIDs,
			characteristicUUIDs,
			onServicesAndCharacteristicsDiscovered
		);
	});
	
	peripheral.on('disconnect', () => {
		console.log('disconnected');
	});
}

function onServicesAndCharacteristicsDiscovered(error, services, characteristics) {
	console.log('Discovered services and characteristics');
	const echoCharacteristic = characteristics[0];
	
	echoCharacteristic.on('data', (data, isNotification) => {
		console.log('Received: ' + data);
	});
	
	echoCharacteristic.subscribe(error => {
		if(error) {
			console.error('Error subscribing to echoCharacteristic');
		} else {
			console.log('Subscribed for echoCharacteristic notifications');
	
			readFromUser(echoCharacteristic);
		}
	});
}

function readFromUser(echoCharacteristic) {
	
	const readline = require('readline').createInterface({
	  input: process.stdin,
	  output: process.stdout
	});
	
	readline.question(`> `, (msg) => {
	  const message = new Buffer(msg);
	  echoCharacteristic.write(message);
	  readline.close();
	  readFromUser(echoCharacteristic);
	});
}